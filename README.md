# README #

Pebble background worker.
Show 5 sec. pop-up notifications, bluetooth and battery state:
- battery icon, by wrist;
- bluetooth icon, by change state.

Runs in the background and do not depend on others running apps and faces.
Takes one apps slot, require activation in "Activity" menu.